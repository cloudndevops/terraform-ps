symbiosis
===========

1) Create s3 bucket

copy the python packages to s3. ansible script will download from s3

pip-19.2.2.tar.gz)
pexpect-4.7.0.tar.gz)
boto3-1.9.207.tar.gz)
setuptools-41.1.0.zip)
s3transfer-0.2.1.tar.gz)
future-0.17.1.tar.gz)
jmespath-0.9.4.tar.gz)
botocore-1.12.207.tar.gz)
PyMySQL-0.9.3.tar.gz)



1) Create an ec2 instance and install ansible, terraform , git and aws cli access - to test the code
2) Create iam role with Full Administrative access and attach to ec2 instance  ( Note: This can be restrict to required resource access to provision)
3) once ec2 provision , Login in to server and create /apps directory and go the directory (cd /apps)
4) download the source code of terraform
git clone https://cloudndevops@bitbucket.org/cloudndevops/terraform-ps.git

5) Download the Ansible Code
git clone https://cloudndevops@bitbucket.org/cloudndevops/ansible-ps.git

6) you can generate public and private key (or) you can make use of the key which you have crated in step1

7) upload the public key in 

8) Copy the private key to /apps directory ( it will be usedful when you run the ansible scripts)

9) create  VPC
This will create VPC network with required subnets, nacls, nat and igw

cd /apps/terraform-ps/projects/symbiosis/main-vpc-accounts/prod

terraform init
terraform plan
terraform apply

10) create  ec2 and rds

cd /apps/terraform-ps/projects/symbiosis/environments/prod

terraform init
terraform plan
terraform apply

11) once provisioned. run ansible playbook to install the applicaition

cd /apps/ansible-ps

 ansible-playbook symbiosis.yml -i hosts --key-file "/apps/key"




